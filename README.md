# PIW - Advanced API Usage

All demos in the PIW were run against a small mock api implemented in `app.py`. Data is read from a file called `records.json`. The provided `records.json` file contains 10'000 randomly generated user profiles. You can create your own users by running the `generate.py` script. Have a look inside to customize the user model. 

## Setup 

0. (Optional) Setup virtual environment and activate it
```
$ python3 -m venv venv
$ source venv/bin/activate
```
1. Install requirements
```
$ python3 -m pip install -r requirements.txt
```
2. Start mock API server
```
$ python3 app.py
```

Done. The mock api should now respond on port 7070. You can now have a look at the scripts provided in the `pagination/`, `parallel/` and `rate_limiting` folder and run them for yourself.  