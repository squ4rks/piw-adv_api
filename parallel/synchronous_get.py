import requests
import time

start = time.time()
for page in range(0, 100):
    url = "http://127.0.0.1:7070/api/v0/users"
    params = {
        'page': page,
        'size': 100
    }
    resp = requests.get(url, params=params)
    print(f"{page}: {resp.status_code}")
end = time.time()
print(f"Time: {end - start}")
