import aiohttp
import asyncio
import time

start = time.time()

async def main():
    async with aiohttp.ClientSession() as session:
        for page in range(0, 100):
            url = f'http://127.0.0.1:7070/api/v0/users'
            params = {
                'size': 100,
                'page': page
            }
            async with session.get(url, params=params) as resp:
                status = await resp.json()
                print(f"{page}: {resp.status}")

asyncio.run(main())
end = time.time()
print(f"Time: {end - start}")