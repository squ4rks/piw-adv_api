import requests

def get_result(url, session=requests.Session()):
    resp = session.get(url)

    if resp.ok:
        return resp.json()
    else:
        raise Exception(f"Failed to retrieve {url} - Status code: {resp.status_code}")

def get_paginated_result(url, session=requests.Session(), container='items'):
    next_page = url

    while next_page is not None:
        res = get_result(next_page, session)

        for itm in res.get(container, []):
            yield itm
        next_page = res['links']['next']

url = "http://127.0.0.1:7070/api/v0/users"
for obj in get_paginated_result(url):
    print(obj)