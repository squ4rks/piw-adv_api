import json
import uuid
import names
import random
import datetime

# Generate records
start_date = datetime.datetime(year=1952, month=1, day=1)
end_date = datetime.datetime(year=2002, month=1, day=1)

days_between = (end_date - start_date).days

records = []
for i in range(0, 10000):
    gender = random.choice(['male', 'female'])

    days = random.randrange(days_between)
    birthday = start_date + datetime.timedelta(days=days)

    u = {
        'id': str(uuid.uuid4()),
        'firstName': names.get_first_name(gender=gender),
        'lastName': names.get_last_name(),
        'birthday': str(birthday.strftime('%Y-%m-%d'))
    }
    print(u)
    records.append(u)
json.dump(records, open("records.json", "w"), indent=2)
