import json
import math

from flask import Flask, jsonify, request
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address



records = json.load(open('records.json'))
index = {}
for idx in range(0, len(records)):
    record = records[idx]
    index[record['id']] = idx

app = Flask(__name__)
limiter = Limiter(
    app,
    key_func=get_remote_address,
    retry_after='delta-seconds',
    headers_enabled=True
)

@app.route('/api/v0/users')
@limiter.limit('500 per minute')
def users():
    size = request.args.get('size', 100)
    size = int(size)
    if size > 250:
        size = 250
    page = request.args.get('page', 1)
    page = int(page)
    if page <= 0:
        page = 1
    # Calculate number of chunks
    num_chunks = math.ceil(len(records)/size)

    # Get chunk of data
    chunks = []
    for i in range(0, len(records), size):
        chunks.append(records[i:i+size])

    # Get links
    base = request.root_url
    links = {}
    next_page = None
    if (page + 1) < num_chunks:
        next_page = page + 1

    if next_page: 
        links['next'] = f"{base}api/v0/users?size={size}&page={next_page}"
    else:
        links['next'] = None

    prev_page = None
    if (page - 1) > 0:
        prev_page = page - 1

    if prev_page:
        links['prev'] = f"{base}api/v0/users?size={size}&page={prev_page}"
    else:
        links['prev'] = None

    ret = {
        'links': links,
        'count': len(records),
        'items': chunks[page - 1]
    }

    return jsonify(ret)

@app.route('/api/v0/users/<id>')
@limiter.limit('1 per minute')
def user(id):
    if id in index.keys():
        idx = index[id]
        return records[idx]
    else:
        return {'error': "No user with id"}, 404

app.run(host='0.0.0.0', port=7070)

