import requests
import time

url = f"http://127.0.0.1:7070/api/v0/users/ee41aef1-d595-45ab-8954-f9f2a6d2ab74"

def do_request(url, method, json={}, sess=requests.Session()):
    while True:
        resp = sess.request(method, url, json=json)

        if resp.status_code == 429:
            try:
                wait = int(resp.headers['Retry-After'])
            except ValueError:
                print(f"Retry-After header is no integer. Aborting.")
                return None
            time.sleep(wait)
            continue
        else:
            return resp

resp = do_request(url, "get")
print(resp)


